CREATE TABLE tutorials (
    id BIGINT(20) NOT NULL PRIMARY KEY,
    description VARCHAR(255),
    published BIT(1) DEFAULT 0,
    title VARCHAR(255)
);
