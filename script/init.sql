USE testdb;

CREATE TABLE tutorials (
    id bigint(20) NOT NULL PRIMARY KEY,
    description varchar(255),
    published bit(1),
    title varchar(255)
);
